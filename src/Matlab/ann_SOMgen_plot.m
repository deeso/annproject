% ann_SOMgen_plot
% this is incomplete as of the moment and not functioning

% INTRO:
%   This just saves the weights, generates and saves the SOM figures

% save the weights
string = ['',image_name,'_',num2str(it),'_w.mat'];
save(string,'w');

figure(1); clf; % this is the quick plot for original debugging 
if size_data(1) == 2
    plot(w(1,:),w(2,:),'.')
elseif size_data(1) == 3
    plot3(w(1,:),w(2,:),w(3,:),'.')
end
axis(ax)
string = ['',image_name,'_',num2str(it),'_nodes.fig'];
saveas(gcf,string)
string = ['',image_name,'_',num2str(it),'_nodes.pdf'];
saveas(gcf,string)
string = ['',image_name,'_',num2str(it),'_nodes.png'];
saveas(gcf,string)

% generate the figure this will contain thick lines for the edges
figure(2); clf; 
axis(ax)
if size_PE(2) == 1 % if 1D SOM
    hold on
    if size_data(1) == 2
        plot(w(1,:),w(2,:),'r-')
        hold on
    elseif size_data(1) == 3
        plot3(w(1,:),w(2,:),w(3,:),'r-')
        hold on
    end
elseif size_PE(2) == 2 % if 2D SOM
    hold on
    max_row = dim(2);
    max_col = dim(1);
    % plot all rows 
    for iii = 0:(max_row-1)
        if (iii == 0) || ( iii == max_row-1)
            if size_data(1) == 2
                plot(w(1,iii*dim(1)+1:(iii+1)*dim(1)),...
                     w(2,iii*dim(1)+1:(iii+1)*dim(1)),'r-','LineWidth',3)
                hold on
            elseif size_data(1) == 3
               plot3(w(1,iii*dim(1)+1:(iii+1)*dim(1)),...
                     w(2,iii*dim(1)+1:(iii+1)*dim(1)),...
                     w(3,iii*dim(1)+1:(iii+1)*dim(1)),'r-','LineWidth',3)
                hold on; axis(ax);
            end
        else
            if size_data(1) == 2
                plot(w(1,iii*dim(1)+1:(iii+1)*dim(1)),...
                     w(2,iii*dim(1)+1:(iii+1)*dim(1)),'r-')
                hold on
            elseif size_data(1) == 3
                plot3(w(1,iii*dim(1)+1:(iii+1)*dim(1)),...
                      w(2,iii*dim(1)+1:(iii+1)*dim(1)),...
                      w(3,iii*dim(1)+1:(iii+1)*dim(1)),'r-')
                hold on; axis(ax);
            end
        end
    end
    % plot all cols
    for jjj = 1:max_col
        if (jjj == 1) || (jjj == max_col)
            if size_data(1) == 2
                plot(w(1,jjj:dim(2):mod_PE(2)),...
                     w(2,jjj:dim(2):mod_PE(2)),'r-','LineWidth',3)
                hold on
            elseif size_data(1) == 3
                plot3(w(1,jjj:dim(2):mod_PE(2)),...
                      w(2,jjj:dim(2):mod_PE(2)),...
                      w(3,jjj:dim(2):mod_PE(2)),'r-','LineWidth',3)
                hold on; axis(ax);
            end
        else
            if size_data(1) == 2
                plot(w(1,jjj:dim(2):mod_PE(2)),...
                     w(2,jjj:dim(2):mod_PE(2)),'r-')
            elseif size_data(1) == 3
                plot3(w(1,jjj:dim(2):mod_PE(2)),...
                      w(2,jjj:dim(2):mod_PE(2)),...
                      w(3,jjj:dim(2):mod_PE(2)),'r-')
                hold on; axis(ax);
            end
        end
    end
elseif size_PE(2) == 3 % If 3DSOM
    max_row = dim(2)*dim(3);
    max_col = dim(1);
    % plot all rows
    for iii = 0:(max_row-1)
        if (iii < dim(2)) || (mod(iii,dim(2)) == 0)  || ...
           (mod(iii+1,dim(2)) == 0) || (iii > max_row-2 - dim(2))
            if size_data(1) == 3
                plot3(w(1,(iii*dim(1)+1):((iii+1)*dim(1))), ...
                      w(2,(iii*dim(1)+1):((iii+1)*dim(1))), ...
                      w(3,(iii*dim(1)+1):((iii+1)*dim(1))),'r-','LineWidth',3)
                hold on;
            elseif size_data(1) == 2
                plot(w(1,(iii*dim(1)+1):((iii+1)*dim(1))), ...
                     w(2,(iii*dim(1)+1):((iii+1)*dim(1))),'r-','LineWidth',3)
                 hold on
            end
        else
             if size_data(1) == 3
                 plot3(w(1,(iii*dim(1)+1):((iii+1)*dim(1))), ...
                       w(2,(iii*dim(1)+1):((iii+1)*dim(1))), ...
                       w(3,(iii*dim(1)+1):((iii+1)*dim(1))),'r-')
                 hold on
             elseif size_data(1) == 2
                 plot(w(1,(iii*dim(1)+1):((iii+1)*dim(1))), ...
                      w(2,(iii*dim(1)+1):((iii+1)*dim(1))),'r-')
                  hold on
             end
        end
    end
    % plot all cols
    for kkk = 0:(dim(3)-1)
        kkk = kkk*mod_PE(2);
        for jjj = 1:max_col
            if (jjj == 1) || (jjj == max_col) || (kkk == 0) || ...
               (kkk == (dim(3)-1)*mod_PE(2))
                if size_data(1) == 3
                    plot3(w(1,jjj+kkk:dim(2):mod_PE(2)+kkk),...
                          w(2,jjj+kkk:dim(2):mod_PE(2)+kkk),...
                          w(3,jjj+kkk:dim(2):mod_PE(2)+kkk),'r-','LineWidth',3)
                    hold on; axis(ax);
                elseif size_data(1) == 2
                    plot(w(1,jjj+kkk:dim(2):mod_PE(2)+kkk),...
                         w(2,jjj+kkk:dim(2):mod_PE(2)+kkk),'r-','LineWidth',3)
                    hold on; axis(ax);
                end
            else
                if size_data(1) == 3
                    plot3(w(1,jjj+kkk:dim(2):mod_PE(2)+kkk),...
                          w(2,jjj+kkk:dim(2):mod_PE(2)+kkk),...
                          w(3,jjj+kkk:dim(2):mod_PE(2)+kkk),'r-')
                    hold on; axis(ax);
                elseif size_data(1) == 2
                    plot(w(1,jjj+kkk:dim(2):mod_PE(2)+kkk),...
                         w(2,jjj+kkk:dim(2):mod_PE(2)+kkk),'r-')
                    hold on; axis(ax);
                end
            end
        end
    end
    % plot all towers
    for iii = 1:mod_PE(2)
            if (iii <= dim(1)) || (iii >= mod_PE(2)-dim(1)) || ...
               (mod(iii,dim(1)) == 0) || (mod(iii-1,dim(1)) == 0)
                if size_data(1) == 3
                    plot3(w(1,iii:mod_PE(2):mod_PE(3)),...
                          w(2,iii:mod_PE(2):mod_PE(3)),...
                          w(3,iii:mod_PE(2):mod_PE(3)),'r-','LineWidth',3)
                    hold on
                elseif size_data(1) == 2
                    plot(w(1,iii:mod_PE(2):mod_PE(3)),...
                          w(2,iii:mod_PE(2):mod_PE(3)),'r-','LineWidth',3)
                    hold on
                end
            else
                if size_data(1) == 3
                    plot3(w(1,iii:mod_PE(2):mod_PE(3)),...
                          w(2,iii:mod_PE(2):mod_PE(3)),...
                          w(3,iii:mod_PE(2):mod_PE(3)),'r-')
                    hold on
                elseif size_data(1) == 2
                    plot(w(1,iii:mod_PE(2):mod_PE(3)),...
                         w(2,iii:mod_PE(2):mod_PE(3)),'r-')
                    hold on
                end
            end
    end
end
string = ['',image_name,'_',num2str(it),'_SOM.fig'];
saveas(gcf,string)
string = ['',image_name,'_',num2str(it),'_SOM.pdf'];
saveas(gcf,string)
string = ['',image_name,'_',num2str(it),'_SOM.png'];
saveas(gcf,string)

% now add topology violations to the figure:
for iii = 1:no_PE
    hold on;
    a = w(:,iii);
    wtmp = w;
    wtmp(:,iii) = inf*a;
    err = bsxfun(@minus,wtmp,a);
    [min_err b] = min(sum(err.^2,1));
    xa = location(iii,mod_PE,size_PE);
    xb = location(b,mod_PE,size_PE);
    xba = xb-xa;
    dba = xba*xba';
    if dba > sqrt(2)
        if size_data(1) == 3
            plot3([w(1,iii),w(1,b)],...
                  [w(2,iii),w(2,b)],...
                  [w(3,iii),w(3,b)],'b--','LineWidth',3)
            hold on
        elseif size_data(1) == 2
            plot([w(1,iii),w(1,b)],...
                 [w(2,iii),w(2,b)],'b--','LineWidth',3)
            hold on
        end
    end
end
hold off
string = ['',image_name,'_',num2str(it),'_top.fig'];
saveas(gcf,string)
string = ['',image_name,'_',num2str(it),'_top.pdf'];
saveas(gcf,string)
string = ['',image_name,'_',num2str(it),'_top.png'];
saveas(gcf,string)