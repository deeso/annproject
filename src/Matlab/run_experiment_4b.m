%Experiment #3 where the 2D data is inputted 
% into the following SOM topologies 8x8
clear all; close all; clc;
basedir = 'experiments\\3\\a'

weights_final_fmt = '%s\\2D_%dx%d_PEs_final_weights.mat'
image_name_fmt = '%s\\2D_%dx%d_PEs'


dims = {[4 4 4];} 

it_max = 1e6;                         % max iteration
alpha = [0.5 0.3 0.2 0.1  0.05;
            100 500 1e3 5e4  inf]; % learning rate and dimensions
nbh_flag = 2;                         % guassian
lattice = 0;                          % cubic lattice
sigma = [1     1    1    1;
            1     1    1    1;
            1     1    1    1;
            4     3    2    1;
            1e3  1e4 1e5 inf];      % the value of sigma


m = 1e3;                                % save every 1e3 steps
ax = [0 1];                      % arguments axis limitation




load('3D_Data.mat')
datas = {E; E144; T; T144; BANG; SPHERE; CUBE;}
p = {'E'; 'E144'; 'T'; 'T144'; 'bang'; 'sphere'; 'cube';}

for i = 1:size(p)
    data = datas{i}';
    data = data(1:2, :);
    ax = [min(data(1, :)), max(data(1, :)), ...
          min(data(2, :)), max(data(2, :))];
          
    dir = p{i};
    cdir = sprintf('%s/%s', basedir, dir);
    for j = size(dims)
        dim = {dims(j)};
        image_name = sprintf(image_name_fmt, cdir, dim(1), dim(2));
        w = ann_SOMgen(data,dim,it_max,alpha,nbh_flag,lattice,sigma,image_name,m,ax);
        weights_final_name = sprintf(weights_final_fmt, cdir, dim(1), dim(2));
        save(weights_final_name, w);
    end
end

