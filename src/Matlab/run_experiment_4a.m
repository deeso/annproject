%Experiment #4 where the 3D data is inputted 
% into the following SOM topologies a) 2 x1 x32, 2x2x 16, 4x4x4, 2x8x4
clear all; close all; clc;
basedir = 'experiments\\3\\a'

weights_final_fmt = '%s\\3D_%dx%dx%d_PEs_final_weights.mat'
image_name_fmt = '%s\\3D_%dx%dx%d_PEs'


dims = {[2 1 32]; [4 4 4]; [2 2 16 ]; [2 8 4]; [4 2 8];} 

dim_sz = [50, 75, 100]
it_max = 1e6;                         % max iteration
alpha = [0.5 0.3 0.2 0.1  0.05;
            100 500 1e3 5e4  inf]; % learning rate and dimensions
nbh_flag = 2;                         % guassian
lattice = 0;                          % cubic lattice
sigma = [1     1    1    1;
            1     1    1    1;
            1     1    1    1;
            4     3    2    1;
            1e3  1e4 1e5 inf];      % the value of sigma


m = 1e3;                                % save every 1e3 steps




load('3D_Data.mat')
datas = {E; E144; T; T144; BANG; SPHERE; CUBE;}
p = {'E'; 'E144'; 'T'; 'T144'; 'bang'; 'sphere'; 'cube';}

for i = 1:size(p)
    data = datas{i}';
    ax = [min(data(1, :)), max(data(1, :)), ...
          min(data(2, :)), max(data(2, :)), ...
          min(data(3, :)), max(data(3, :))];
          
    dir = p{i};
    cdir = sprintf('%s/%s', basedir, dir);
    for j = size(dims)
        dim = {dims(j)};
        image_name = sprintf(image_name_fmt, cdir, dim(1), dim(2), dim(3));
        w = ann_SOMgen(data,dim,it_max,alpha,nbh_flag,lattice,sigma,image_name,m,ax);
        weights_final_name = sprintf(weights_final_fmt, cdir, dim(1), dim(2), dim(3));
        save(weights_final_name, w);
    end
end

