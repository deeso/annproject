clear all; close all; clc;
basedir = '~/git/annproject/src/Matlab/experiments/2/b/'


dim_sz = [50, 75, 100]
it_max = 1e6;                         % max iteration
alpha = [0.5 0.3 0.2 0.1  0.05;
            100 500 1e3 5e4  inf]; % learning rate and dimensions
nbh_flag = 2;                         % guassian
lattice = 0;                          % cubic lattice
sigma = [1     1    1    1;
            1     1    1    1;
            1     1    1    1;
            4     3    2    1;
            1e3  1e4 1e5 inf];      % the value of sigma


m = 1e3;                                % save every 1e3 steps
ax = [0 1];                      % arguments axis limitation


%#2 b)
%  50 - 100, and after 1E6 steps topology violation
%  50, 75, 100 - PE

load('3D_Data.mat')
datas = {E; E144; T; T144; BANG; SPHERE; CUBE;}
dirs = {'E'; 'E144'; 'T'; 'T144'; 'bang'; 'sphere'; 'cube';}

for i = 1:size(dirs)
    data = datas{i}';
    data = data(1:2, :);
    ax = [min(data(1, :)), max(data(1, :)), min(data(2, :)), max(data(2, :))];
    dir = dirs{i};
    cdir = sprintf('%s/%s', basedir, dir);
    for j = size(dim_sz)
        dim = [dim_sz(j)];
        image_name = sprintf('%s/1D_%d_PEs', cdir, dim_sz(j));
        w = ann_SOMgen(data,dim,it_max,alpha,nbh_flag,lattice,sigma,image_name,m,ax);
        weights_final_name = sprintf('%s/1D_%d_PEs_final_weights.mat', cdir, dim_sz(j));
        save(weights_final_name, w);
    end
end
