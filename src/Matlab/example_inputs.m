% ann_SOMgen test
clear all; close all; clc;
%% example of 3D with spherical gaussian

data = rand(2,4000);            % 2D~U(0,1)x4000
dim = [8 8 8];                    % size of the SOM
it_max = 1e6;                   % max iteration
alpha = [0.5 0.3 0.2 0.1  0.05;
         100 500 1e3 5e4  inf]; % learning rate and dimensions
nbh_flag = 2;                   % guassian
lattice = 0;                    % cubic lattice
sigma = [1    1   1   1;
         1    1   1   1;
         1    1   1   1;
         4    3   2   1;
         1e3  1e4 1e5 inf];     % the value of sigma
     
image_name=['data\\ann_SOMgen_test']; % the string to test with
m = 1e3;                        % save every 1e3 steps
ax = [0 1 0 1];                 % arguments axis limitation

w = ann_SOMgen(data,dim,it_max,alpha,nbh_flag,lattice,sigma,image_name,m,ax);

%% example fo 2D with spherical gaussian

% data = rand(3,4000);            % 2D~U(0,1)x4000
% dim = [5 5];                    % size of the SOM [5,2] did funky stuff
% it_max = 1e6;                   % max iteration
% alpha = [0.5 0.3 0.2 0.1  0.05;
%          100 500 1e3 5e4  inf]; % learning rate and dimensions
% nbh_flag = 2;                   % guassian
% lattice = 0;                    % cubic lattice
% sigma = [1    1   1   1;
%          1    1   1   1;
%          4    3   2   1;
%          100  500 1e3 inf];     % the value of sigma
% image_name=['ann_SOMgen_test']; % the string to test with
% m = 1e4;                        % save every 1e3 steps
% ax = [0 1 0 1 0 1];                 % arguments axis limitation
% 
% w = ann_SOMgen(data,dim,it_max,alpha,nbh_flag,lattice,sigma,image_name,m,ax);

%% example of 1D with spehrical gaussian

% data = rand(3,4000);            % 2D~U(0,1)x4000
% dim = [35];                    % size of the SOM [5,2] did funky stuff
% it_max = 1e6;                   % max iteration
% alpha = [0.5 0.3 0.2 0.1  0.05;
%          100 500 1e3 5e4  inf]; % learning rate and dimensions
% nbh_flag = 2;                   % guassian
% lattice = 0;                    % cubic lattice
% sigma = [1    1   1   1;
%          4    3   2   1;
%          100  500 1e3 inf];     % the value of sigma
% image_name=['ann_SOMgen_test']; % the string to test with
% m = 1e4;                        % save every 1e3 steps
% ax = [0 1 0 1 0 1];                 % arguments axis limitation
% 
% w = ann_SOMgen(data,dim,it_max,alpha,nbh_flag,lattice,sigma,image_name,m,ax);