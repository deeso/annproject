function [w] = ann_SOMgen(data,dim,it_max,alpha,nbh_flag,lattice,sigma,image_name,m,ax)

% INPUTS:
%   data - the data for training (column organized)
%   PE_dim - the dimensions of the SOM
%   it_max - the maximum learning steps
%   alpha - the leanring rate and the domain of the values (column organized)
%       [a1     a2      a3      a4
%        100    1000    10000   inf]
%   nbh_flag - the flag for the neighborhood
%       0 - square
%       1 - diamond
%       2 -  guassian
%   lattice - the type of lattice
%       0 - cubic
%       1 - hexagonal
%   sigma - the configuration of the neighborhood (column organized)
%       [s1x     s2x      s3x   s4x
%        s1y     s2y      s3y   s4y
%        s1z     s2z      s3z   s4z
%       sig1    sig2     sig3  sig4
%        100    1000    10000   inf]
%   image_name - the title to save these images under
%   m - how often to save the plots and weights
%
% OUTPUTS:
%   w - weights of the final outputs
%   Plots and figures aswell as weights every m steps

% initialize the network
size_data = size(data);
no_PE = 1; mod_PE = [];
size_PE = size(dim);
for ii = 1:size_PE(2)
    no_PE = no_PE*dim(ii);
    mod_PE = [mod_PE no_PE];
end
w = rand(size_data(1),no_PE);

% initialization for the loop
it = 0;
disp(['current learning step: ',num2str(it)]);
figure(7);clf % this is the quick plots
axis(ax)
if size_data(1) == 2
    plot(data(1,:),data(2,:),'.')
elseif size_data(1) == 3
    plot3(data(1,:),data(2,:),data(3,:),'.')
end
string = ['',image_name,'_data.fig'];
saveas(gcf,string)
string = ['',image_name,'_data.pdf'];
saveas(gcf,string)
string = ['',image_name,'_data.png'];
saveas(gcf,string)

run ann_SOMgen_plot % TODO: finish
% note: ann_SOMgen_plot is a SUBROUTINE and uses image_name

while it < it_max
    it = it+1;
    
    % select a random data point
    X = data(:,round(1+(size_data(2)-1)*rand));
    
    % select the winner
    err = bsxfun(@minus,w,X);
    [min_err winner] = min(sum(err.^2,1));
    
    % determine the location of the winner
    x = location(winner,mod_PE,size_PE);
    
    % update the weights with the neighborhood function
    w = update(x,X,mod_PE,size_PE,w,it,alpha,nbh_flag,lattice,sigma); % TODO: test and finish
    
    % every so often plot the results
    if mod(it,m) == 0
        disp(['current learning step: ',num2str(it)]);
        run ann_SOMgen_plot % TODO: finish
    end
    
end

return