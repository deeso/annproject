function w = update(x,X,mod_PE,size_PE,w,it,alpha,nbh_flag,lattice,sigma)

% INPUTS:
%   x - the winner location
%   X - the current data point
%   mod_PE - used to help generate the PE location
%   size_PE - size(dim)
%   w - weight matrix
%   it - iteration count
%   alpha - the leanring rate and the domain of the values (column organized)
%       [a1     a2      a3      a4
%        t1     t2      t3      t4] 
%   nbh_flag - the flag for the neighborhood
%       0 - square
%       1 - diamond
%       2 - spherical guassian
%       3 - asymetric guassian
%   lattice - the type of lattice
%       0 - cubic
%       1 - hexagonal
%   sigma - the configuration of the neighborhood (column organized)
%       [s1x     s2x      s3x   s4x
%        s1y     s2y      s3y   s4y
%        s1z     s2z      s3z   s4z
%        t1    	 t2    	  t3    t4] 
% OUPUTS:
%   updated w matrix

% get current learning rate
size_alpha = size(alpha);
for ii = 1:size_alpha(2)
    if it < alpha(end,ii)
        cur_alpha = alpha(1,ii);
    end
end

if (nbh_flag == 0) % TODO: square function

elseif (nbh_flag == 1) % TODO: diamond function
    
elseif (nbh_flag == 2) || (nbh_flag == 3)
    % get the sigma value 
    size_sigma = size(sigma);
    for ii = 1:size_sigma(2);
        if it < sigma(end,ii)
            strech_factor = sigma(1:(end-2),ii)';
            cur_sigma = sigma(end-1,ii);
        end
    end
    % generate the distance to the winner! (only important for guassian)
    dist = zeros(1,mod_PE(end));
    if lattice == 0 % if square lattice
        for ii = 1:mod_PE(end)
            tmp = location(ii,mod_PE,size_PE);
            dist(ii) = sqrt((tmp-x.*strech_factor)*(tmp-x.*strech_factor)');
        end
    elseif lattice == 1 % TODO: write the distance for hex
    end
    % apply the guassian
    if nbh_flag == 2
        % apply the guassian
        dist = exp(-dist.^2/(2*cur_sigma));
    end
    
end

% update the weights
w = w - cur_alpha*bsxfun(@times,dist,bsxfun(@minus,w,X));

return