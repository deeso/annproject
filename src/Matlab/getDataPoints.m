function data = getDataPoints(numPoints, type)
    data = -1 * ones(numPoints, 3);
    if (strcmpi('cube', type))
        data = getCube(numPoints);
    elseif (strcmpi('E', type))
        data = getEShape(numPoints);
    elseif (strcmpi('E144', type))
        data = getE144Shape(numPoints);
    elseif (strcmpi('T', type))
        data = getTShape(numPoints);
    elseif (strcmpi('T144', type))
        data = getT144Shape(numPoints);
    elseif (strcmpi('sphere', type))
        data = getSphere(numPoints);
    elseif (strcmpi('!', type))
        data = getExclamationShape(numPoints);
    end
    
end
    

function data = getCube(numPoints)
    volume  = 1
    x = nthroot(volume, 3) * rand(numPoints, 1);
    y = nthroot(volume, 3) * rand(numPoints, 1);
    z = nthroot(volume, 3) * rand(numPoints, 1);
    data = [x y z];
end

function data = getSphere(numPoints)
    volume = 1
    theta = 360 * rand(numPoints, 1);
    phi = 180 * rand(numPoints, 1);
    rho = nthroot(3/(4 * pi) * volume, 3) * rand(numPoints, 1);
    [x,y,z] = sph2cart(theta,phi,rho);
    data = [x y z];
end



function data = getE144Shape(numPoints)
    %x_Start, y_Start, x_Lenght, y_Length, volume
    rect1 = [12*3, 12*10, 12*5, 12*2, 144*2*5];
    rect2 = [12*3, 12*6,  12*4, 12*2, 144*2*4];
    rect3 = [12*3, 12*1,  12*5, 12*2, 144*2*5];
    rect4 = [12*1, 12*1,  12*2, 12*11, 144*2*12];
    
    volumet = sum([rect1(5), rect2(5), rect3(5), rect4(5)]);
    v_prop1 = rect1(5)/volumet;
    v_prop2 = rect4(5)/volumet;
    
    v = rand(numPoints, 1);
    z = rand(numPoints, 1);
    y = rand(numPoints, 1);
    x = rand(numPoints, 1);
    for i = 1:numPoints
        if (v(i) < v_prop1)
            y(i) = rect1(2) + y(i) * rect1(4);
            x(i) = rect1(1) + x(i) * rect1(3);
        elseif (v(i) < 2*v_prop1)
            y(i) = rect2(2) + y(i) * rect2(4);
            x(i) = rect2(1) + x(i) * rect2(3);       
        elseif (v(i) < 3*v_prop1)
            y(i) = rect3(2) + y(i) * rect3(4);
            x(i) = rect3(1) + x(i) * rect3(3);
        else
            y(i) = rect4(2) + y(i) * rect4(4);
            x(i) = rect4(1) + x(i) * rect4(3);
        end
    end
    data = [x y z];
    return;
end
function data = getEShape(numPoints)
    %x_Start, y_Start, x_Lenght, y_Length, volume
    rect1 = [3, 10, 5, 2, 2*5];
    rect2 = [3, 6, 4, 2, 2*4];
    rect3 = [3, 1, 5, 2, 2*5];
    rect4 = [1, 1, 2, 11, 2*12];
    
    volumet = sum([rect1(5), rect2(5), rect3(5), rect4(5)]);
    v_prop1 = rect1(5)/volumet;
    v_prop2 = rect4(5)/volumet;
    
    v = rand(numPoints, 1);
    z = rand(numPoints, 1);
    y = rand(numPoints, 1);
    x = rand(numPoints, 1);
    for i = 1:numPoints
        if (v(i) < v_prop1)
            y(i) = rect1(2) + y(i) * rect1(4);
            x(i) = rect1(1) + x(i) * rect1(3);
        elseif (v(i) < 2*v_prop1)
            y(i) = rect2(2) + y(i) * rect2(4);
            x(i) = rect2(1) + x(i) * rect2(3);       
        elseif (v(i) < 3*v_prop1)
            y(i) = rect3(2) + y(i) * rect3(4);
            x(i) = rect3(1) + x(i) * rect3(3);
        else
            y(i) = rect4(2) + y(i) * rect4(4);
            x(i) = rect4(1) + x(i) * rect4(3);
        end
    end
    data = [x y z];
    return;
end

function data = getTShape(numPoints)
    %x_Start, y_Start, x_Lenght, y_Length, volume
    rect1 = [2, 10, 10, 2, 2*10];
    rect2 = [6, 1, 2, 9, 2*9];
    
    volumet = sum([rect1(5), rect2(5)]);
    v_prop1 = rect1(5)/volumet;
    v_prop2 = rect2(5)/volumet;
    
    v = rand(numPoints, 1);
    z = rand(numPoints, 1);
    y = rand(numPoints, 1);
    x = rand(numPoints, 1);
    for i = 1:numPoints
        if (v(i) < v_prop1)
            y(i) = rect1(2) + y(i) * rect1(4);
            x(i) = rect1(1) + x(i) * rect1(3);
        else
            y(i) = rect2(2) + y(i) * rect2(4);
            x(i) = rect2(1) + x(i) * rect2(3);       
        end
    end
    data = [x y z];
    return;
end

function data = getT144Shape(numPoints)
    %x_Start, y_Start, x_Lenght, y_Length, volume
    rect1 = [2*12, 10*12, 10*12, 2*12, 144*2*10];
    rect2 = [6*12, 1, 2*12, 10*12, 144*2*10];
    
    volumet = sum([rect1(5), rect2(5)]);
    v_prop1 = rect1(5)/volumet;
    v_prop2 = rect2(5)/volumet;
    
    v = rand(numPoints, 1);
    z = rand(numPoints, 1);
    y = rand(numPoints, 1);
    x = rand(numPoints, 1);
    for i = 1:numPoints
        if (v(i) < v_prop1)
            y(i) = rect1(2) + y(i) * rect1(4);
            x(i) = rect1(1) + x(i) * rect1(3);
        else
            y(i) = rect2(2) + y(i) * rect2(4);
            x(i) = rect2(1) + x(i) * rect2(3);       
        end
    end
    data = [x y z];
    return;
end


function data = getExclamationShape(numPoints)
    %x_Start, y_Start, x_Lenght, y_Length, volume
    rect1 = [2, 1, 2, 2, 2 * 2];
    rect2 = [2, 5, 2, 5, 5 * 2];
    volumet = sum([rect1(5), rect2(5)]);
    v_prop1 = rect1(5)/volumet;
    v_prop2 = rect2(5)/volumet;
    
    v = rand(numPoints, 1);
    z = rand(numPoints, 1);
    y = rand(numPoints, 1);
    x = rand(numPoints, 1);
    for i = 1:numPoints
        if (v(i) < v_prop1)
            y(i) = rect1(2) + y(i) * rect1(4);
            x(i) = rect1(1) + x(i) * rect1(3);
        else
            y(i) = rect2(2) + y(i) * rect2(4);
            x(i) = rect2(1) + x(i) * rect2(3);
        end
    end
    data = [x y z];
    return;
end


