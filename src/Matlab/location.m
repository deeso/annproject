function x = location(winner,mod_PE,size_PE)

% INPUTS:
%   winner - the winner from the arg min function
%   mod_PE - used to help generate the PE location
%   size_PE - size(dim)
%
% OUTPUTS:
%   x - the location in SOM space

x = zeros(1,size_PE(2));

if size_PE(2) == 3
    x(3) = floor((winner-1)/mod_PE(2));
    winner = winner - x(3)*mod_PE(2);
    x(1) = floor((winner-1)/mod_PE(1));
    winner = winner - x(1)*mod_PE(1);
    x(2) = (winner-1);
    x = x+1;
elseif size_PE(2) == 2
    x(1) = floor((winner-1)/mod_PE(1));
    winner = winner - x(1)*mod_PE(1);
    x(2) = (winner-1);
    x = x+1;
elseif size_PE(2) == 1
    x(1) = winner;
end

return