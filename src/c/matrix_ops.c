// Author: Adam Pridgen <adam.pridgen@rice.edu>
//
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

/*   Parameters:
 *      ary - pointer to the contiguous block of N values where N = the total
 *            number of entries that match the matrix's dimensions
 *      i - ith row in the matrix
 *      j - jth column in the matrix
 *      k - kth matrix for k > 1 (e.g. 3 dimensional matrix)
 *      l - lth matrix element l > 1 (e.g. 4D matrix reserved for weights of
 *        SOM)
 *      max_i - max row size of matrix
 *      max_j - max column size (important only for 3-d matrix)
 *      max_k - max height size of matrix (important only for 4D matrix)
 *      value - value to be written into the matrix
 *
 */

inline double four_dim_write(double *ary, unsigned int i, unsigned int j, unsigned int k,
        unsigned int l, unsigned int max_i, unsigned int max_j, unsigned int max_k,
        double value){
	int combine = i*max_i + j*max_j + k*max_k + l;
	*(ary + (i*max_i*max_j*max_k + j*max_j*max_k + k*max_k + l)) = value; 
}

inline double four_dim_read(double *ary, unsigned int i, unsigned int j, unsigned int k,
        unsigned int l, unsigned int max_i, unsigned int max_j, unsigned int max_k){
	return *(ary + (i*max_i*max_j*max_k + j*max_j*max_k + k*max_k + l));

}

inline double three_dim_write(double *ary,  unsigned int i, unsigned int j, unsigned int k,
                           unsigned int max_i, unsigned int max_j, double value){
    int combine = i * max_i + j * max_j + k;
    *(ary + (i * max_i * max_j + j * max_j + k)) = value;
}

inline double three_dim_read(double *ary,  unsigned int i, unsigned int j, unsigned int k,
             unsigned int max_i, unsigned int max_j){
    return *(ary + (i * max_i * max_j + j * max_j + k));
}


inline double two_dim_write(double *ary,  unsigned int i, unsigned int j, 
                           unsigned int max_i, double value){
    // 0 - is the value of k, 1- change to account for the fact that max_j must be at least 1
    three_dim_write(ary, i, j, 0, max_i, 1, value);  
}

inline double two_dim_read(double *ary,  unsigned int i, unsigned int j, unsigned int max_i){
    return three_dim_read(ary, i, j, 0, max_i, 1);
}
