#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "matrix_ops.h"
#define MAX_X 10
#define MAX_Y 10
#define MAX_Z 10




int main(){
   double *array3d = (double *) malloc(MAX_X * MAX_Y * MAX_Z * sizeof(double));
   double *array3d2 =(double *) malloc(MAX_X * MAX_Y * MAX_Z * sizeof(double));

   double *array = (double *) malloc(MAX_X * MAX_Y * sizeof(double));
   double *array2 =(double *) malloc(MAX_X * MAX_Y * sizeof(double));
   int cnt = 0, cnt2 = 0, i = 0, j = 0, k = 0;
   double v = 0;
   for (i = 0; i < MAX_X; i++){
       for (j = 0; j < MAX_Y; j++){
           v = i*MAX_X + j ;
           *(array + i*MAX_X + j) = v;
           two_dim_write(array2, i, j, MAX_X, v);
           cnt++;
       }
   }
   for (i = 0; i < MAX_X; i++){
       for (j = 0; j < MAX_Y; j++){
           for ( k = 0; k < MAX_Z; k++){
               v = i*MAX_X * MAX_Y + j * MAX_Y + k ;
               *(array3d + i*MAX_X*MAX_Y + j * MAX_Y + k ) = v ;
               three_dim_write(array3d2, i, j, k, MAX_X, MAX_Y, v);
               printf("Reading location %d from array3d2, should be: %d but is %f array3d = %f\n",
                       i * MAX_X + j * MAX_Y + k, cnt2, 
                       *(array3d + i * MAX_X * MAX_Y+ j * MAX_Y + k),
                       *(array3d2 + i * MAX_X * MAX_Y+ j * MAX_Y + k));
               cnt2++;
           }
       }
   }

  
   printf("array[%d][%d] = %f, you said: %d\n", 9, 9, *(array +99), cnt );
   printf("array[%d][%d] = %f, you said: %f\n", 9, 9, *(array + 9 * MAX_X + 9), 
         two_dim_read(array,9,9,MAX_X));
   printf("array[%d][%d] = %f, you said: %f\n", 9, 9, *(array + 9 * MAX_X + 9), 
         two_dim_read(array2,9,9,MAX_X));
   
   for (i = 0; i < MAX_X; i++){
       for (j = 0; j < MAX_Y; j++){
           assert(*(array +99) == *(array2 +99));
           //printf("Reading location %d from array2, should be: %f\n",i * MAX_X + j, *(array + i * MAX_X + j));
           //printf("Reading location %d from array2, should be: %f\n",i * MAX_X + j, *(array2 + i * MAX_X + j));
           assert(*(array + i * MAX_X + j) == two_dim_read(array2, i, j, MAX_X));
       }
   }
   for (i = 0; i < MAX_X; i++){
       for (j = 0; j < MAX_Y; j++){
           for ( k = 0; k < MAX_Z; k++){
               v = i*MAX_X + j * MAX_Y + k ;
               assert(*(array3d +99) == *(array3d2 +99));
           printf("Reading location %d from array3d2, should be: %f\n",
                       i * MAX_X + j * MAX_Y + k, *(array3d + i * MAX_X + j * MAX_Y + k));
           printf("Reading location %d from array3d2, should be: %f\n",
                       i * MAX_X + j * MAX_Y + k, *(array3d2 + i * MAX_X + j * MAX_Y + k));
               assert(*(array3d + i * MAX_X * MAX_Y + j * MAX_Y + k) == three_dim_read(array3d2, i, j, k, MAX_X, MAX_Y));
           }
       }
   }
   return 0;
}
