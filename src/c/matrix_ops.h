// Author: Adam Pridgen <adam.pridgen@rice.edu>
//
/*   Parameters:
 *      ary - pointer to the contiguous block of N values where N = the total
 *            number of entries that match the matrix's dimensions
 *      i - ith row in the matrix
 *      j - jth column in the matrix
 *      k - kth matrix for k > 1 (e.g. 3 dimensional matrix)
 *	l - lth matrix element l > 1 (e.g. 4D matrix reserved for weights of 
 *	  SOM)
 *      max_i - max row size of matrix
 *      max_j - max column size (important only for 3-d matrix)
 *	max_k - max height size of matrix (important only for 4D matrix)
 *      value - value to be written into the matrix
 *
 */
inline double four_dim_write(double *ary, unsigned int i, unsigned int j, unsigned int k,
	unsigned int l, unsigned int max_i, unsigned int max_j, unsigned int max_k, 
	double value);
inline double four_dim_read(double *ary, unsigned int i, unsigned int j, unsigned int k,
unsigned int l, unsigned int max_i, unsigned int max_j, unsigned int max_k);

inline double three_dim_write(double *ary,  unsigned int i, unsigned int j, unsigned int k,
                           unsigned int max_i, unsigned int max_j, double value);
inline double three_dim_read(double *ary,  unsigned int i, unsigned int j, unsigned int k,
             unsigned int max_i, unsigned int max_j);

inline double two_dim_write(double *ary,  unsigned int i, unsigned int j, unsigned int max_i,double value);
inline double two_dim_read(double *ary,  unsigned int i, unsigned int j, unsigned int max_i);
