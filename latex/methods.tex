\newpage
\section{Methods and Approach}\label{sec:methods}

\subsection{Data}\label{sec:methods_data}

All the data for this project was synthetically generated from a
uniform random distribution via Matlab's {\tt random} function.  Each
shape was created as a 3D silhouette composed of 50x10$^3$ data points
between 0 and 1.  When two dimensions were needed, the {\it z-dimension}
was dropped.  The shapes included
a cube, and a sphere.  These shapes were constructed using a fixed volume of 1.  In addition to the shapes, we
also created data that consisted of three characters (e.g. `E', `T',
and `!' also known as {\it bang})).  These points were drawn from the
space in 12x12x1.% space.% within the silhouette.
\spar
The cube and sphere were selected because these shapes are regular, and
there regularity offers a measure of certainty when trying to interpret
our results.  Specifically, these shapes helped us understand that the
effects we observed in the various configurations were not anomalies.
The characters were specially selected due to their characteristics.
For instance, the bang character represents a discontinuous shape, and
the `T'
represents two rectangles that are perpendicular to each other, and thus
there are three points that can be represented as relatively distant
from each other.  In the case of the `E', we were aiming to see how well
the SOM handle this data space.     

%\newline
\begin{figure} [h]
  \caption{2D and 3D renderings of the of our synthetic data.}
  \begin{center}$
  \begin{array} {l l}
  \subfloat[2D bang  ]{\includegraphics[trim = 20mm 10mm 15mm 15mm, clip,width=0.37\textwidth]  {../results/data/2D_bang_data}} &
  \subfloat[3D bang  ]{\includegraphics[trim = 20mm 10mm 15mm 15mm, clip,width=0.37\textwidth]  {../results/data/3D_bang_data}} \\
  \subfloat[2D cubic ]{\includegraphics[trim = 20mm 10mm 15mm 15mm, clip,width=0.37\textwidth]  {../results/data/2D_cube_data}} &
  \subfloat[2D cubic ]{\includegraphics[trim = 20mm 10mm 10mm 15mm, clip,width=0.37\textwidth]  {../results/data/3D_cube_data}} \\
  \end{array}$
  \end{center}
\end{figure} \clearpage \newpage 
\begin{figure}
  \caption{2D and 3D renderings of the of our synthetic data continued.}
  \begin{center}$
  \begin{array} {l l}
  \subfloat[2D E     ]{\includegraphics[trim = 20mm 10mm 15mm 15mm, clip,width=0.4\textwidth]     {../results/data/2D_E_data}} &
  \subfloat[3D E     ]{\includegraphics[trim = 20mm 10mm 15mm 15mm, clip,width=0.4\textwidth]     {../results/data/3D_E_data}} \\
  \subfloat[2D sphere]{\includegraphics[trim = 20mm 10mm 15mm 15mm, clip,width=0.4\textwidth]{../results/data/2D_sphere_data}} &
  \subfloat[3D sphere]{\includegraphics[trim = 20mm 10mm 15mm 15mm, clip,width=0.4\textwidth]{../results/data/3D_sphere_data}} \\
  \subfloat[2D T     ]{\includegraphics[trim = 20mm 10mm 15mm 15mm, clip,width=0.4\textwidth]     {../results/data/2D_T_data}} &
  \subfloat[3D T     ]{\includegraphics[trim = 20mm 10mm 15mm 15mm, clip,width=0.4\textwidth]     {../results/data/3D_T_data}} \\
  \end{array}$
  \end{center}
\end{figure} 
 \clearpage  \newpage

\subsection{Experiments}\label{sec:methods_exp1}
Below is the suite of experiments performed over the course of this
project.  As a brief overview of the work conducted, 77 experiments 
were performed over 10 days on 2 machines continuously
running each day.  The machines were limited to running two experiments
simultaneously each over this period of time.  These experimental
constraints were due to a lack of memory even though each machine had 8
GB of RAM available.  The results for the experiments totaled in excess of 60
GB of data. 
\begin{description}
\item[Experiment 1: Neighborhood function experiments] \hspace*{\fill} \\
By inhibiting the ability of the 3D SOM to communicate in the
z-direction, determine the point where the 3D SOM acts as multiple 2D
SOMs. To reduce the SOM communication, we will scale the spherical
Gaussian in the z-direction applied to the cubic 2D and 3D data for this experiment.

\item[Experiment 2: Identify and demonstrate 1D SOM Lattice topology
violations] \hspace*{\fill}\\ 
\begin{description}  \vspace*{-20pt}
\item[a.]  Determine the maximum number of PEs before topology violations occur
in all of the data sets. We will do this by starting with 2 PEs and
adding an additional one each time the SOM succeeds.
\item[b.]  Demonstrate that a topology violation occurs in all of the datasets
if the number of PEs in the SOM exceeds a certain value.
\end{description}

\item[Experiment 3: Identify and demonstrate 2D SOM Lattice topology
violations] \hspace*{\fill}\\ 
\begin{description}  \vspace*{-20pt}
\item[a.]  Vary the aspect ratio of the SOM to determine when the SOM breaks
topology preservation.  The SOM will have a constant number of 64 PEs.
This experiment will be conducted on all data sets, but their topology
will be varied.
\item[b.]  Determine if the SOM violates topology on 3D data set like the 1D
SOM did when the 2D dataset was used as input like the demonstration
shown in class.  This will be performed using a 8x8 SOM configuration
and using the 3D datasets as input.
\end{description}

\item[Experiment 4: Identify and demonstrate 3D SOM Lattice topology
violations] \hspace*{\fill}\\ 
\begin{description}  \vspace*{-20pt}
\item[a.]  Vary the aspect ratios of the SOM to determine when the SOM
breaks topology preservation. This experiment will be conducted with a
constant number of 64 PEs. This experiment will be conducted on all data
sets.
\item[b.]  Determine if the SOM violates topology on 2D datasets.
This will be performed using a 4x4x4 SOM configuration and using the 2D
datasets as input. 
\end{description}

\end{description}


\subsection{Network Parameters}\label{sec:methods_NP}


\begin{table}[htbp] 

\center
\caption{Table shows the overall network parameters for all of our experiments. }
  \label{tab:NP}
  \scalebox{0.9}{
\begin{tabular}{p{4cm} p{.1cm} l}
  \toprule
  \multicolumn{3}{l}{\bf Learning Parameters} \\
  \bottomrule \noalign{\smallskip}
  Initial weights & & Uniform Distribution in [0, 1] \\
  Alpha (learning rate) & & Varied via a learning schedule specified by the following function with x as the learning step. \\
	& & $\alpha(x) = \begin{cases} 0.5, & \text{if }x\text{ x $<$ 100} \\
						   	    0.3, & \text{if }x\text{ x $<$ 500} \\
						   	    0.2, & \text{if }x\text{ x $<$ 1,000}\\
						   	    0.1, & \text{if }x\text{ x $\geq$ 1,000}\\ \end{cases}$\\
  Neighborhood function & & Varied via a learning schedule specified by the following function with x as the learning step. \\
	& & $\Theta(x) = N(\mu_{winner},\sigma^2)$ \\
	& & $\sigma(x) = \begin{cases} 4, & \text{if }x\text{ x $<$ 1,000} \\
						   	    3, & \text{if }x\text{ x $<$ 10,000} \\
						   	    2, & \text{if }x\text{ x $<$ 100,000}\\
						   	    1, & \text{if }x\text{ x $\geq$ 100,000}\\ \end{cases}$\\
  Max learning steps & & 1,000,000 \\
  Training set & & 50,000 training points of the specified data set\\ 
  \toprule
  \multicolumn{3}{l}{\bf Quality Measures } \\
  \bottomrule \noalign{\smallskip}
  m (\# of learn steps \newline between recording) & &  1,000 \\
  Topology Violation & & When the the $n^{th}$ PE is closer in data space than all the closer PEs in SOM space. \\
  \bottomrule \noalign{\smallskip}
\end{tabular}}
\end{table} 


\begin{table}[htbp] 

\center
\caption{Table shows the varied datasets and SOM sizes for all the
experiments.}
  \label{tab:SOMS}
  \scalebox{0.9}{
\begin{tabular}{p{4cm} p{.1cm} l}
  \toprule
  \multicolumn{3}{l}{\bf Experiment 1 Lattice Sizes} \\
  \bottomrule \noalign{\smallskip}
   & & 3D SOM 5x5x5\\
  \toprule
  \multicolumn{3}{l}{\bf Experiment 1 Datasets} \\
  \bottomrule \noalign{\smallskip}
   & & 3D cubic dataset\\
  \toprule
  \multicolumn{3}{l}{\bf Experiment 2 Lattice Sizes} \\
  \bottomrule \noalign{\smallskip}
   & & 1D SOM ranging from 2 to 10 PEs\\
  \toprule
  \multicolumn{3}{l}{\bf Experiment 2 Datasets} \\
  \bottomrule \noalign{\smallskip}
   & & All datasets were used \\ 
  \toprule
  \multicolumn{3}{l}{\bf Experiment 3 Lattice Sizes} \\
  \bottomrule \noalign{\smallskip}
  Experiment 3a & & 2D SOM restricted to 64 PEs \\
   & & 1x64 x32, 4x16 \\
  Experiment 3b & & 8x8 2D SOM \\
  \toprule
  \multicolumn{3}{l}{\bf Experiment 3 Datasets} \\
  \bottomrule \noalign{\smallskip}
  Experiment 3a & & All 3D datasets were converted to 2D \\ 
  Experiment 3b & & All 3D datasets were used \\
  \toprule
  \multicolumn{3}{l}{\bf Experiment 4 Lattice Sizes} \\
  \bottomrule \noalign{\smallskip}
  Experiment 4a & & 3D SOM restricted to 64 PEs \\
   & & 2x2x16, 2x4x8 \\
  Experiment 4b & & 4x4x4 3D SOM \\
  \toprule
  \multicolumn{3}{l}{\bf Experiment 4 Datasets} \\
  \bottomrule \noalign{\smallskip}
  Experiment 4a & & All 3D datasets were used \\
  Experiment 4b & & All 3D datasets were converted to 2D \\
  \bottomrule \noalign{\smallskip}
\end{tabular}}
\end{table} 



Table~\ref{tab:NP} shows an overview of all the experimental parameters
applied applied in our experiments, and Table~\ref{tab:SOMS} shows how each
of the SOMs were varied in sized along with how their input data is
varied.  
